﻿using Microsoft.OData;
using Microsoft.OData.Edm;
using Microsoft.OData.UriParser;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.OData.Routing.Conventions;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using VirkKon.Models;

namespace VirkKon
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes (probably not used, as the OData mapping takes precedence)
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            ODataModelBuilder builder = new ODataConventionModelBuilder(config);
            builder.EntitySet<Contact>("Contacts");
            IEdmModel model = builder.GetEdmModel();

            config.MapODataServiceRoute(
                routeName: "ODataRoute",
                routePrefix: null,
                model: model);

            // Enable specific OData queries:
            config.Select().Expand().Filter().OrderBy().MaxTop(null).Count();
        }
    }
}

/* To allow attribute based routing (mark each method with attributes that tell the url they match).
 * And enable alternate keys.
 * https://stackoverflow.com/questions/41929367/how-to-set-enable-alternate-keys-with-microsoft-aspnet-odata-version-6
 * 
 * IList<IODataRoutingConvention> routeconvs = ODataRoutingConventions.CreateDefaultWithAttributeRouting("odata", config); // "odata" is route contact, whatever that is
 * config.MapODataServiceRoute(
 *           routeName: "odata",     // route name, arbitrary
 *                       routePrefix: null,      // the start of the URL path
 *                       configureAction: containerBuilder =>   // what is configureAction?
 *                                               containerBuilder
 *                                                   .AddDefaultODataServices()
 *                                                   .AddService(ServiceLifetime.Singleton, s => model)
 *                                                   .AddService<IEnumerable<IODataRoutingConvention>>(ServiceLifetime.Singleton, sp => routeconvs)
 *                                                  .AddService<ODataUriResolver>(ServiceLifetime.Singleton, s => new AlternateKeysODataUriResolver(model))
 *                        );
 */
