﻿/* 


See comments at the beginning of Controllers\ClientAuthAttribute.cs


----------------------------------------------------------------------

Pitfall #8 – Key Contains a Period

Calling the OData service to get a person by email address returns 404 Not Found.
Solution

Email addresses contain periods.  A period in an HTTP URL normally identifies the file extension and IIS uses the extension to identify which module to route requests to.
Change this behavior so that Web API gets a chance to route the request by adding the following setting to <system.webServer> in web.config:

<modules runAllManagedModulesForAllRequests="true" />

Requests for static content will experience a performance hit so this setting might not be appropriate for all servers.

From https://blogs.msdn.microsoft.com/davidhardin/2014/12/17/web-api-odata-v4-lessons-learned/
 


*/