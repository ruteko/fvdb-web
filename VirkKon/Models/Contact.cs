﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

// This model is used by both OData and Entity Framework.
// We want DataOwnersId to be the Key in OData, where we'll silently add DataOwner to get the real key.
// VirkKonId is also a unique key, but we don't use that.

// This strange use of keys means that we can't use Entity Framework's defaults, so we override them in VkDb.cs

namespace VirkKon.Models
{
    public partial class Contact
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? VirkKonId { get; set; }

        [StringLength(40)]
        public string DataOwner { get; set; }

        [Key]
        [StringLength(40)]
        public string DataOwnersId { get; set; }

        public long CompPNumber { get; set; }

        [StringLength(255)]
        public string CompPersonFirstName { get; set; }

        [StringLength(255)]
        public string CompPersonLastName { get; set; }

        [StringLength(255)]
        public string CompPersonPhone { get; set; }

        [StringLength(255)]
        public string CompPersonEmail { get; set; }

        [StringLength(255)]
        public string CompPersonTitle { get; set; }

        [StringLength(255)]     // nvarchar(max) in the db, but our spec says 255.
        public string CompComment { get; set; }

        [StringLength(40)]
        public string CompCenterRelation { get; set; }

        public long CenterPNumber { get; set; }

        [StringLength(255)]
        public string CenterPersonFirstName { get; set; }

        [StringLength(255)]
        public string CenterPersonLastName { get; set; }

        [StringLength(255)]
        public string CenterPersonPhone { get; set; }

        [StringLength(255)]
        public string CenterPersonEmail { get; set; }

        [StringLength(255)]
        public string CenterPersonTitle { get; set; }

        [Column(TypeName = "date")]
        public DateTime ContactDate { get; set; }

        [Required]
        [StringLength(40)]
        public string ContactType { get; set; }

        [StringLength(40)]
        public string ContactSubject { get; set; }
    }
}

/* We might change to use DataOwner explicitly in OData, like this: Contacts(DataOwner='xxx',DataOwnersId='yyy')
 * Or define VirkKonId as the main key, and DataOwner+DataOwnersId as alternate key.
 * But alternate keys seem difficult in OData: https://groups.google.com/forum/#!topic/odata-discussion/QKdOhw25LiY
 */
