﻿using System.Data.Entity;
using System.Data.SqlClient;

namespace VirkKon.Models
{
    public partial class VkDb : DbContext
    {
        public VkDb()
            : base("name=VkDb")
        {
        }

        public virtual DbSet<DataOwner> DataOwners { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Contacts must use sprocs for create/update/delete, because it's a view of multiple tables,
            // and to ensure that we don't use DataOwnersId as key on its own.
            // https://msdn.microsoft.com/en-us/library/dn468673(v=vs.113).aspx
            modelBuilder.Entity<Contact>()
                  .MapToStoredProcedures(s =>
                  {
                      s.Insert(i => i.HasName("Contact_Upsert"));
                      s.Update(u => u.HasName("Contact_Upsert"));
                      s.Delete(d => d.HasName("doesnt_exist"));     // We can't map Delete, as our key is wrong; just make sure to get an error if ever invoked.
                  });
        }

        // A manual way to delete a contact
        public void Contact_Delete(string dataowner, string dataownersid)
        {
            var dopar = new SqlParameter("@DataOwner", dataowner);
            var doidpar = new SqlParameter("@DataOwnersId", dataownersid);
            this.Database.ExecuteSqlCommand("exec Contact_Delete @DataOwner, @DataOwnersId", dopar, doidpar);
        }
    }
}
