﻿using System.ComponentModel.DataAnnotations;

namespace VirkKon.Models
{
    public partial class DataOwner
    {
        [Key]
        [StringLength(40)]
        public string Token { get; set; }

        public int Cvr { get; set; }
    }
}