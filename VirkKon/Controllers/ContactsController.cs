﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using System.Web.OData.Query;
using System.Web.OData.Extensions;
using VirkKon.Models;
using VirkKon.Util;

namespace VirkKon.Controllers
{
    // ClientAuthAttribute checks the client certificate for all methods in this class
    [ClientAuth]
    public class ContactsController : ODataController
    {
        VkDb db = new VkDb();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private bool ContactExists(string dataOwner, string dataOwnersId)
        {
            return db.Contacts.Any(p => (p.DataOwnersId == dataOwnersId && p.DataOwner == dataOwner));
        }

        // Get all results
        [EnableQuery(AllowedQueryOptions = AllowedQueryOptions.All)]
        public IQueryable<Contact> Get()
        {
            return db.Contacts.AsQueryable();
        }

        //// Get one using Id
        //[EnableQuery]
        //public SingleResult<ContactEvent> Get([FromODataUri] int id)
        //{
        //    IQueryable<ContactEvent> result = db.ContactEvents.Where(p => p.VirkKonId == id);
        //    return SingleResult.Create(result);
        //}

        //// Get results from a P-number
        //[EnableQuery]
        //public IQueryable<Contact> Get([FromODataUri] long pno)
        //{
        //    IQueryable<Contact> result = db.Contacts.Where(p => p.CompPNumber == pno).OrderByDescending(r => r.ContactDate);
        //    return result.AsQueryable();
        //}

        // Post a new record
        public IHttpActionResult Post(Contact ce)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            ce.DataOwner = CertUtil.GetDataOwner(ActionContext);
            if (ContactExists(ce.DataOwner, ce.DataOwnersId))
                return Conflict();
            db.Contacts.Add(ce);
            db.SaveChanges();
            return Created(ce);
        }

        // Put, update all fields in the record
        public IHttpActionResult Put([FromODataUri] string key, Contact ce)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            ce.DataOwner = CertUtil.GetDataOwner(ActionContext);
            // Disallow changing of DataOwnersId (actually, we might allow that at some point).
            if (key != ce.DataOwnersId)
                return BadRequest();
            if (!ContactExists(ce.DataOwner, key))
                return NotFound();
            db.Entry(ce).State = EntityState.Modified;
            db.SaveChanges();
            return Updated(ce);
        }

        // Patch some fields in the record
        public IHttpActionResult Patch([FromODataUri] string key, Delta<Contact> dce)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            string dataOwner = CertUtil.GetDataOwner(ActionContext);
            if (!ContactExists(dataOwner, key))
                return NotFound();
            Contact ce = db.Contacts.Single(p => (p.DataOwnersId == key && p.DataOwner == dataOwner));
            dce.Patch(ce);
            ce.DataOwner = dataOwner;   // Make sure we don't change DataOwner
            db.SaveChanges();
            return Updated(ce);
        }

        // Delete one record
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            string dataOwner = CertUtil.GetDataOwner(ActionContext);
            if (!ContactExists(dataOwner, key))
                return NotFound();
            db.Contact_Delete(dataOwner, key);
            // Ordinarily, you would do this, but that requires the key to be correctly defined:
            // db.Contacts.Remove(ce);
            // db.SaveChanges();
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}