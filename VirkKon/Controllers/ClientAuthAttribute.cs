﻿// This checks the client certs on those methods that are decorated with [ClientAuth].
//
// You must setup IIS to accept or require client certs, and add the Nets DanID root as trusted to the computer certificate store.
//
// IIS will validate that the client cert is OK and trusted; here we check that the cert is specifically for this system,
// and we extract the DataOwner from it.

using System;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using VirkKon.Models;
using VirkKon.Util;

// There appears to be a bug in IIS 10 that client certs will only work if you install the Windows component
// IIS/WWW Services/Security/IIS Client Certificate Mapping Auth.  Then disable it in web.config:
//      <system.webServer><security><authentication>
//          <iisClientCertificateMappingAuthentication enabled="false" />
//      </ authentication ></ security ></ system.webServer>
// This web.config section will need to be unlocked; this can be done in the IIS Manager, Configuration Editor after choosing
// both "Section" and "From" at the top.

// For test situations, you can disable revocation check on the client cert by adding this DWord to Regedit and rebooting:
// HKLM\SYSTEM\CurrentControlSet\Services\HTTP\Parameters\SslBindingInfo\[site]\DefaultSslCertCheckMode = 1

// Testing with Firefox doesn't show any http status code - this appears to be a Firefox-bug of no consequence to us.


namespace VirkKon.Controllers
{
    public class ClientAuthAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext ctx)
        {
            X509Certificate2 cert = ctx.Request.GetClientCertificate();

            if (cert == null)
            {
                SetResponse(ctx, HttpStatusCode.Forbidden, "Klientcertifikat mangler.");
                return;
            }

            // We only want certs issued by Nets DanID.
            // This test is a bit simplistic, but IIS has already blocked any issuer not trusted by Windows.
            if (!cert.Issuer.Contains("TRUST2408"))
            {
                SetResponse(ctx, HttpStatusCode.Forbidden, "Klientcertifikat er udstedt af " + cert.Issuer + ", men skal være fra Nets DanID.");
                return;
            }

            int? cvr = CertUtil.ExtractCvr(cert.Subject);
            string owner = CertUtil.ExtractDataOwner(cert.Subject);

            if (!cvr.HasValue)
            {
                SetResponse(ctx, HttpStatusCode.Forbidden, "Klientcertifikat er udstedt til " + cert.Subject + ". Ikke genkendt som et funktionscertifikat.");
                return;
            }
            if (owner == "")
            {
                SetResponse(ctx, HttpStatusCode.Forbidden, "Klientcertifikat er udstedt til " + cert.Subject + ". Ikke genkendt som beregnet til VirkKon.");
                return;
            }

            try
            {
                // Maybe it's a bit slow to instantiate a new DB object here. Could we move this to the Controller?
                using (VkDb db = new VkDb())
                {
                    if (!db.DataOwners.Any(p => (p.Token == owner && p.Cvr == cvr.Value)))
                    {
                        SetResponse(ctx, HttpStatusCode.Forbidden, "Klientcertifikat er udstedt til " + cert.Subject + ". Vi har ikke registreret denne som tilmeldt systemet.");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                SetResponse(ctx, HttpStatusCode.InternalServerError, ex.Message + " " + ex.StackTrace);
                return;
            }

            base.OnAuthorization(ctx);
        }

        private void SetResponse(HttpActionContext ctx, HttpStatusCode status, string content)
        {
            // It seems we can't use ReasonPhrase here, as it doesn't work on IIS 10 with HTTPS:
            // https://social.technet.microsoft.com/Forums/windows/en-US/1fbbcd89-910e-4645-9577-d36307d3206d/iis-will-not-return-reasonphrase-over-https-on-windows-server-2016windows-10?forum=WinServerPreview
            // It's probably better to pass the detailed error message as content anyway.
            ctx.Response = new HttpResponseMessage(status);
            ctx.Response.Content = new StringContent(content);
        }
    }
}