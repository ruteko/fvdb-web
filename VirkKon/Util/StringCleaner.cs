﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirkKon.Util
{
    public static class StringCleaner
    {
        // Remove control characters and superfluous whitespace
        public static string CleanSingleLine(string str)
        {
            if (str == null)
                return null;

            // Whitespace to space
            str = str.Replace('\u0009', ' ');   // tab
            str = str.Replace('\u000a', ' ');   // lf
            str = str.Replace('\u0085', ' ');   // unicode next line
            str = str.Replace('\u2028', ' ');   // unicode line separator
            str = str.Replace('\u2029', ' ');   // unicode paragraph separator

            // remove all other control chars
            str = new string(str.Where(c => !char.IsControl(c)).ToArray());

            // remove double spaces
            string str2 = "";
            while (str2 != str)
            {
                str2 = str;
                str = str.Replace("  ", " ");
            }

            return str.Trim();
        }

        // Remove control characters except newline, which is converted to the normal LF.
        // We don't try to handle classic Mac CR alone.
        // Also remove superfluous whitespace.
        public static string CleanMultiLine(string str)
        {
            if (str == null)
                return null;

            // Whitespace to space
            str = str.Replace('\u0009', ' ');   // tab

            // Unicode line breaks to ordinary
            str = str.Replace('\u0085', '\u000a');   // unicode next line
            str = str.Replace('\u2028', '\u000a');   // unicode line separator
            str = str.Replace('\u2029', '\u000a');   // unicode paragraph separator

            // remove all control chars except lf
            str = new string(str.Where(c => c == '\u000a' || !char.IsControl(c)).ToArray());

            // remove double spaces
            string str2 = "";
            while (str2 != str)
            {
                str2 = str;
                str = str.Replace("  ", " ");
            }

            // remove spaces next to lf
            str = str.Replace("\u000a ", "\u000a");
            str = str.Replace(" \u000a", "\u000a");

            return str.Trim();
        }
    }
}