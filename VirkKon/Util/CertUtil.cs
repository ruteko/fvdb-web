﻿using System.Net.Http;
using System.Web.Http.Controllers;

namespace VirkKon.Util
{
    public static class CertUtil
    {

        // Utilities for the Danish NemID Funktionscertifikat

        // Example of the X509Certificate2.Subject from a function cert:
        //
        // SERIALNUMBER=CVR:27555373-FID:64445418 + CN=VirkKon-IDS (funktionscertifikat), O=ID SOLUTIONS ApS // CVR:27555373, C=DK
        //
        // We want to check the CVR, which is guaranteed to identify the subject company.
        // And we want to check that Common Name starts with VirkKon, to make sure the cert is for this system.
        // The CN part after the dash is our DataOwner.
        //
        // Because other parts of the system assures that we'll only get real function certificates from the official issuer, we
        // make no attempts to handle differently formated subject lines.

        // Check that the subject starts with a CVR, and if so, return it.
        public static int? ExtractCvr(string subj)
        {
            if (subj.StartsWith("SERIALNUMBER=CVR:"))
            {
                string cvrstr = subj.Substring("SERIALNUMBER=CVR:".Length, 8);
                int cvr;
                if (int.TryParse(cvrstr, out cvr))
                    return cvr;
            }
            return null;
        }

        // Check that the Common Name starts with VirkKon, and if so, return the DataOwner part of it.
        public static string ExtractDataOwner(string subj)
        {
            int start = subj.IndexOf("CN=VirkKon-");
            if (start >= 0)
            {
                int end = subj.IndexOf(" (funktionscertifikat)", start);
                if (end >= 0)
                    return subj.Substring(start + "CN=VirkKon-".Length, end - start - "CN=VirkKon-".Length);
                else
                    return subj.Substring(start + "CN=VirkKon-".Length);    // IDS test cert.
            }
            return "";
        }

        public static string GetDataOwner(HttpActionContext ctx)
        {
            return ExtractDataOwner(ctx.Request.GetClientCertificate().Subject);
        }
    }
}
